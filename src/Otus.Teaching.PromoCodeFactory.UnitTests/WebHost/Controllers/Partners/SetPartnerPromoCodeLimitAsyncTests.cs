﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly Fixture _fixture;
        
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnerRepositoryMock = new Mock<IRepository<Partner>>();
            _partnersController = new PartnersController(_partnerRepositoryMock.Object);
            _fixture = new Fixture();
            _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => _fixture.Behaviors.Remove(b));
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }
        
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFoundResult()
        {
            // Arrange
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(
                It.IsAny<Guid>())).ReturnsAsync(null as Partner);
            var guid = new Guid();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            var expectedResult = new NotFoundResult();
            
            // Act
            var actualResult = await _partnersController.SetPartnerPromoCodeLimitAsync(guid, request);
            
            // Assert
            actualResult.Should().BeEquivalentTo(expectedResult);
        }
        
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsBlocked_ReturnsBadRequest()
        {
            // Arrange
            var guid = new Guid();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, false)
                .Create();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(
                It.IsAny<Guid>())).ReturnsAsync(partner);
            var expectedResult = new BadRequestResult();
            
            // Act
            var actualResult = await _partnersController.SetPartnerPromoCodeLimitAsync(guid, request);
            
            // Assert
            actualResult.Should().BeEquivalentTo(expectedResult);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-100)]
        public async Task SetPartnerPromoCodeLimitAsync_LimitLessThanOrEqualToZero_ReturnsBadRequest(int incorrectLimit)
        {
            // Arrange
            var guid = new Guid();
            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, incorrectLimit)
                .Create();
            var expectedResult = new BadRequestResult();
            
            // Act
            var actualResult = await _partnersController.SetPartnerPromoCodeLimitAsync(guid, request);
            
            // Assert
            actualResult.Should().BeEquivalentTo(expectedResult);
        }

        [Theory]
        [InlineData(1, 2)]
        [InlineData(10, 4)]
        [InlineData(100, 25)]
        public async Task SetPartnerPromoCodeLimitAsync_ActiveLimitIsNull_NumberOfLimitsIsNotReset(int numberIssuedPromoCodes, int requestLimit)
        {
            // Arrange
            var guid = new Guid();
            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, requestLimit)
                .Create();
            
            var partner = _fixture.Build<Partner>()
                .With(p => p.Id, guid)
                .With(p => p.NumberIssuedPromoCodes, numberIssuedPromoCodes)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .Create();

            _partnerRepositoryMock.Setup(x =>
                x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            
            _partnerRepositoryMock.Setup(x => 
                x.UpdateAsync(It.IsAny<Partner>())).Verifiable();

            var expectedResultType = typeof(CreatedAtActionResult);
            
            // Act
            var actualResult = await _partnersController.SetPartnerPromoCodeLimitAsync(guid, request);
            
            // Assert
            actualResult.Should().BeOfType(expectedResultType);
            partner?.PartnerLimits?.FirstOrDefault(x => x.Id == guid)?.Limit.Should().Be(requestLimit);
            _partnerRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<Partner>()), Times.Once);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(20)]
        public async Task SetPartnerPromoCodeLimitAsync_ThereIsActiveLimits_ResetsNumberIssuedPromoCodes(int requestLimit)
        {
            // Arrange 
            var guid = new Guid();
            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, requestLimit)
                .Create();
            
            var activePromocodeLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(x => x.CancelDate, null as DateTime?)
                .Create();
            
            var partner = _fixture.Build<Partner>()
                .With(p => p.Id, guid)
                .With(p => p.IsActive, true)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>(){ activePromocodeLimit })
                .Create();
            
            _partnerRepositoryMock.Setup(x =>
                x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            
            _partnerRepositoryMock.Setup(x =>
                x.UpdateAsync(It.IsAny<Partner>())).Verifiable();

            var expectedResultType = typeof(CreatedAtActionResult);
            
            // Act 
            var actualResult = await _partnersController.SetPartnerPromoCodeLimitAsync(guid, request);
            
            // Arrange
            actualResult.Should().BeOfType(expectedResultType);
            partner.NumberIssuedPromoCodes.Should().Be(0);
            partner.PartnerLimits.Count.Should().Be(2);
            activePromocodeLimit.CancelDate.Should().BeCloseTo(DateTime.Now, 500);
            _partnerRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<Partner>()), Times.Once);
        }
    }
}